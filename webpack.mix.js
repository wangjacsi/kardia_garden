let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    /*.styles([
        'resources/assets/app/css/bootstrap.min.css',
        'resources/assets/app/css/animate-headline.css',
        'resources/assets/app/css/owl.carousel.min.css',
        'resources/assets/app/css/pe-icon-7-stroke.css',
        'resources/assets/app/css/font-awesome.min.css',
        'resources/assets/app/css/meanmenu.css',
        'resources/assets/app/css/shortcodes/shortcode.css',
        'resources/assets/app/css/style.css',
        'resources/assets/app/css/responsive.css',
    ], 'public/css/app-plugins.css')*/
   //.sass('resources/assets/sass/app.scss', 'public/css')
   .copy('resources/assets/app/css/', 'public/css', false)
   .scripts([
       'resources/assets/app/js/jquery.meanmenu.js',
       'resources/assets/app/js/isotope.pkgd.min.js',
       'resources/assets/app/js/imagesloaded.pkgd.min.js',
       'resources/assets/app/js/jquery.counterup.min.js',
       'resources/assets/app/js/jquery.waypoints.min.js',
       'resources/assets/app/js/ajax-mail.js',
       'resources/assets/app/js/owl.carousel.min.js',
       'resources/assets/app/js/plugins.js',
       'resources/assets/app/js/main.js'
   ], 'public/js/app-plugins.js')
   .copy('resources/assets/app/plugins/codemirror.min.css', 'public/plugins/codemirror.min.css')
   .copy('resources/assets/app/plugins/codemirror.min.js', 'public/plugins/codemirror.min.js')
   .copy('resources/assets/app/plugins/xml.min.js', 'public/plugins/xml.min.js')
   .copy('resources/assets/app/fonts/', 'public/fonts', false)
   .copyDirectory('resources/assets/app/plugins/froala_editor_2.8.1/', 'public/plugins/froala_editor_2.8.1');
   //.copy('resources/assets/app/plugins/froala_editor_2.8.0/', 'public/plugins/froala_editor_2.8.0', false);
   //.copyDirectory('resources/assets/app/fonts/', 'public/fonts')
   //.copyDirectory('resources/assets/app/plugins/froala_editor_2.8.0/', 'public/plugins/froala_editor_2.8.0');
