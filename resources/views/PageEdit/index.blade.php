@extends('layouts.app')

@section('styles')
@endsection

@section('content')
    @component('components.slider')
    @endcomponent

    @component('components.shop')
    @endcomponent

    @component('components.banner')
    @endcomponent

    @component('components.blog')
    @endcomponent
@endsection

@section('modals')
    @component('modals.login')
    @endcomponent
@endsection


@section('scripts')
<script>

</script>
@endsection
