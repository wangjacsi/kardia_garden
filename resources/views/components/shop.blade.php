<!-- shop area start -->
<div class="shop-area ptb-90">
    <div class="container-fluid">
        <div class="section-title text-center">
            <h3>Featured Collections</h3>
            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
        </div>
        <div class="shop-style-all">
            <div class="row">
                <div class="grid">
                    <div class="col-md-3 col-sm-6 col-xs-12 grid-item cat1">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/1.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="preduct-details.html">Classic Gloves</a></h3>
                                    <span class="new-price">$50</span>
                                    <span class="old-price">$70</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 grid-item cat2 cat3">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/2.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="preduct-details.html">Beige Tote</a></h3>
                                    <span>$80</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 grid-item cat1">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/3.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="preduct-details.html">Classic Gloves</a></h3>
                                    <span>$90</span>
                                    <span class="old-price">$100</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 grid-item cat2">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/4.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="preduct-details.html">Beige Tote</a></h3>
                                    <span>$40</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 grid-item cat1">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/5.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="#">Classic Gloves</a></h3>
                                    <span class="new-price">$50</span>
                                    <span class="old-price">$70</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 grid-item cat3">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/6.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="preduct-details.html">Classic Gloves</a></h3>
                                    <span>$60</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 grid-item cat2 cat3">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/7.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="preduct-details.html">Classic Gloves</a></h3>
                                    <span>$80</span>
                                    <span class="old-price">$90</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 grid-item cat3">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/8.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="preduct-details.html">Classic Gloves</a></h3>
                                    <span>$90</span>
                                    <span class="old-price">$120</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 grid-item cat1">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/9.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="preduct-details.html">Classic Gloves</a></h3>
                                    <span>$70</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 grid-item cat1">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/10.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="preduct-details.html">Classic Gloves</a></h3>
                                    <span>$20</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 hidden-sm col-xs-12 grid-item cat1">
                        <div class="shop hover-style mb-30">
                            <div class="shop-img">
                                <a href="preduct-details.html">
                                    <img src="img/shop/11.jpg" alt="" />
                                </a>
                                <div class="shop-title title-style-1">
                                    <h3><a href="preduct-details.html">Classic Gloves</a></h3>
                                    <span>$30</span>
                                </div>
                                <div class="product-cart">
                                    <a href="#"><i class="pe-7s-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="view-more text-center mt-30">
            <a href="shop.html">view more</a>
        </div>
    </div>
</div>
<!-- shop area end -->
