<!-- slider start -->
<div class="slider-area style-res">
    <div class="container-fluid text-center">
        <div class="slider-opacity ptb-250" style="background-image: url(img/bg/1.jpg)">
            <div class="slider-text style-2 ">
                <h2>summer Collection 2017 </h2>
                <h3>A good looking, comfortable traditional collection</h3>
                <a href="shop.html">Shop The Collection</a>
            </div>
        </div>
    </div>
</div>
<!-- slider end -->
