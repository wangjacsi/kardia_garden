<!-- blog area start -->
<div class="blog-area pb-70">
    <div class="container-fluid">
        <div class="section-title text-center mb-50">
            <h3>Latest News</h3>
            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="single-blog mb-30">
                    <a href="blog-details.html">
                        <img src="img/blog/1.jpg" alt="">
                    </a>
                    <div class="blog-title">
                        <span>December 8, 2017</span>
                        <h3><a href="blog-details.html">Lorem Ipsum is simply dummy</a></h3>
                        <a href="blog-details.html">read more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-blog mb-30">
                    <a href="blog-details.html">
                        <img src="img/blog/2.jpg" alt="">
                    </a>
                    <div class="blog-title">
                        <span>December 8, 2017</span>
                        <h3><a href="blog-details.html">Lorem Ipsum is simply dummy</a></h3>
                        <a href="blog-details.html">read more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-blog mb-30">
                    <a href="blog-details.html">
                        <img src="img/blog/5.jpg" alt="">
                    </a>
                    <div class="blog-title">
                        <span>December 8, 2017</span>
                        <h3><a href="blog-details.html">Lorem Ipsum is simply dummy</a></h3>
                        <a href="blog-details.html">read more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-blog mb-30">
                    <a href="blog-details.html">
                        <img src="img/blog/3.jpg" alt="">
                    </a>
                    <div class="blog-title">
                        <span>December 8, 2017</span>
                        <h3><a href="blog-details.html">Lorem Ipsum is simply dummy</a></h3>
                        <a href="blog-details.html">read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- blog area end -->
