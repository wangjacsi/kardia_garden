<!-- breadcrumb start -->
<div class="breadcrumb-area">
    <div class="container-fluid text-center">
        <div class="breadcrumb-stye gray-bg ptb-100">
            <h2 class="page-title">checkout page</h2>
            <ul>
                <li><a href="#">home</a></li>
                <li><a href="#">shop</a></li>
                <li class="active">checkout</li>
            </ul>
        </div>
    </div>
</div>
<!-- breadcrumb end -->
