<!DOCTYPE html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="shortcut icon" type="image/x-icon" href="/images/logo.ico" />-->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <!--<link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/app-plugins.css') }}" />-->

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pe-icon-7-stroke.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/meanmenu.css') }}">
    <link rel="stylesheet" href="{{ asset('css/shortcodes/shortcode.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

    <!-- Froala Editor -->
    <link rel="stylesheet" href="{{ asset('plugins/codemirror.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.1/css/froala_editor.pkgd.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.1/css/froala_style.min.css') }}">

    @yield('styles')
</head>
<body>
    <div class="waraper" id="froala-editor">

        @include('layouts.header')

        <div id="app">
            @yield('content')
        </div>

        @include('layouts.footer')

        @yield('modals')

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script src="{{ asset('js/app-plugins.js') }}"></script>

    @yield('scripts')

    <!-- Froala Editor -->
    <!-- Include external JS libs. -->
    <script src="{{ asset('plugins/codemirror.min.js') }}"></script>
    <script src="{{ asset('plugins/xml.min.js') }}"></script>

    <!-- Include Editor JS files. -->
    <script src="{{ asset('plugins/froala_editor_2.8.1/js/froala_editor.pkgd.min.js') }}"></script>


    <script>
      $(function() {
        $('div#froala-editor').froalaEditor({
          toolbarInline: true,
          charCounterCount: false,
          dragInline: false,
          toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'embedly', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo'],
          //pluginsEnabled: ['image', 'link', 'draggable']
        })
      });
    </script>
</body>
</html>
