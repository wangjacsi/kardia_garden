<header>
    <div class="header-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-4">
                    <div class="logo">
                        <a href="index.html"><img src="img/logo/logo.png" alt="" class="fr-fil fr-dib"></a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-4  col-xs-2 text-center">
                    <div class="main-menu display-inline">
                        <nav>
                            <ul class="menu">
                                <li><a href="index.html">HOME <i class="pe-7s-angle-down"></i></a>
                                    <ul>
                                        <li><a href="index.html">home version 1</a></li>
                                        <li><a href="index-2.html">home version 2</a></li>
                                        <li><a href="index-3.html">home version 3</a></li>
                                        <li><a href="index-4.html">home version 4</a></li>
                                        <li><a href="index-5.html">home version 5</a></li>
                                        <li><a href="index-6.html">home version 6</a></li>
                                    </ul>
                                </li>
                                <li><a href="about-us.html">ABOUT US</a></li>
                                <li><a href="#">pages <i class="pe-7s-angle-down"></i></a>
                                    <ul>
                                        <li><a href="about-us.html">about us</a></li>
                                        <li><a href="shop.html">shop</a></li>
                                        <li><a href="cart.html">cart</a></li>
                                        <li><a href="login.html">login</a></li>
                                        <li><a href="register.html">register</a></li>
                                        <li><a href="checkout.html">checkout</a></li>
                                        <li><a href="wishlist.html">wishlist</a></li>
                                        <li><a href="preduct-details.html">shop details</a></li>
                                        <li><a href="contact-us.html">contact us</a></li>
                                    </ul>
                                </li>
                                <li><a href="shop.html">shop</a></li>
                                <li><a href="blog.html">BLOG <i class="pe-7s-angle-down"></i></a>
                                    <ul>
                                        <li><a href="blog.html">blog 2 column</a></li>
                                        <li><a href="blog-style-2.html">blog full wide</a></li>
                                        <li><a href="blog-details.html">blog details</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact-us.html">CONTACT</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6 text-right text-sm text-res">
                    <div class="cart-user-language">
                        <div class="shopping-cart ml-30 display-inline">
                            <a class="top-cart" href="cart.html"><i class="pe-7s-cart"></i></a>
                            <ul>
                                <li>
                                    <div class="cart-img">
                                        <a href="#"><img src="img/cart/1.jpg" alt="" /></a>
                                    </div>
                                    <div class="cart-content">
                                        <h3><a href="#"> 1 X Faded...</a> </h3>
                                        <span><b>S, Orange</b></span>
                                        <span class="cart-price">£ 16.84</span>
                                    </div>
                                    <div class="cart-del">
                                        <i class="pe-7s-close-circle"></i>
                                    </div>
                                </li>
                                <li>
                                    <div class="cart-img">
                                        <a href="#"><img src="img/cart/2.jpg" alt="" /></a>
                                    </div>
                                    <div class="cart-content">
                                        <h3><a href="#"> 1 X Faded...</a> </h3>
                                        <span><b>S, Orange</b></span>
                                        <span class="cart-price">£ 16.84</span>
                                    </div>
                                    <div class="cart-del">
                                        <i class="pe-7s-close-circle"></i>
                                    </div>
                                </li>
                                <li>
                                    <div class="shipping">
                                        <span class="f-left">Shipping </span>
                                        <span class="f-right cart-price"> $7.00</span>
                                    </div>
                                    <hr class="shipping-border" />
                                    <div class="shipping">
                                        <span class="f-left"> Total </span>
                                        <span class="f-right cart-price">$692.00</span>
                                    </div>
                                </li>
                                <li class="checkout m-0"><a href="#">checkout <i class="pe-7s-angle-right"></i></a></li>
                            </ul>
                        </div>
                        <div class="user">
                            <a href="#" data-toggle="modal" data-target="#loginModal"><i class="pe-7s-add-user"></i></a>
                        </div>
                        <div class="search-block-top display-inline">
                            <div class="icon-search">
                                <a href="#"><i class="pe-7s-search"></i></a>
                            </div>
                            <div class="toogle-content">
                                <form action="#" id="searchbox">
                                    <input type="text" placeholder="Search" />
                                    <button class="button-search"><i class="pe-7s-search"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile-menu"></div>
        </div>
    </div>
</header>
